package com.outfittery.core;

/**
 * Created by nickc on 9/8/16.
 */
public class LifeGameState {

	public static boolean[][] LIFE_GRID = {{true, false, false, false, false},
			{false, false, false, true, true},
			{false, false, false, true, false},
			{false, true, false, true, false},
			{false, false, true, false, false}
	};
}
