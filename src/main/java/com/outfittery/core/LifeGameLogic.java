package com.outfittery.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import static com.outfittery.core.LifeGameState.LIFE_GRID;

/**
 * Created by nickc on 08/09/16.
 */
@Component
public class LifeGameLogic {

    private static final Logger LOGGER = LoggerFactory.getLogger(LifeGameLogic.class);

    private static final int X_SIZE = 5;
    private static final int Y_SIZE = 5;
    //            10000
//            00011
//            00010
//            01010
//            00100

    private  boolean[][] NEXT_GEN_BOARD = new boolean[X_SIZE][Y_SIZE];
//    private static final boolean[][] LIFE_GRID = new boolean[X_SIZE][Y_SIZE];

    public boolean[][] initLifeGrid() {

        for (int x = 0; x < X_SIZE; x++) {
            for (int y = 0; y < Y_SIZE; y++) {
                int temp = (int) (Math.random() * 4);
                if (temp == 0) {
                    if(isCellAlive(x, y)) {
                        LIFE_GRID[x][y] = true;
                    }
                }
            }
        }
        printBoard();
        copyArray(LIFE_GRID, NEXT_GEN_BOARD);

        return LIFE_GRID;
    }


    public boolean[][] getNextGeneration() {

        for (int x = 1; x < X_SIZE - 1; x++) {
            for (int y = 1; y < Y_SIZE - 1; y++) {

                NEXT_GEN_BOARD[x][y] = isCellAlive(x, y);
            }
        }

        copyArray(NEXT_GEN_BOARD, LIFE_GRID);

        return LIFE_GRID;
    }

    private boolean isCellAlive(int x, int y) {
        int aliveNeighbours = countAliveNeighbours(x, y);
        boolean cellState = LIFE_GRID[x][y];

        if (cellState) {

            if (aliveNeighbours <= 1) {
                cellState = false;
            } else if (aliveNeighbours > 3) {
                cellState = false;
            }
        } else {
            if (aliveNeighbours == 3) {
                cellState = true;
            }
        }

        return cellState;
    }

    public int countAliveNeighbours(int x, int y) {

        int aliveNeighbours = 0;
        for (int i = x - 1; i <= x + 1; i++) {
            for (int j = y - 1; j <= y + 1; j++) {
                if (LIFE_GRID[i][j]) {
                    if (!(i == x && j == y)) {
                        aliveNeighbours++;
                    }
                }
            }

        }

        System.out.print("; x=" + x + " y=" + y + " " + aliveNeighbours);
        return aliveNeighbours;
    }


    private void printBoard() {

        for (int x = 0; x < X_SIZE; x++) {
            for (int y = 0; y < Y_SIZE; y++) {
                if (LIFE_GRID[x][y]) {
                    System.out.print(1);
                } else {
                    System.out.print(0);
                }
            }
            System.out.println();
        }
    }

    private void copyArray(boolean[][] arrayFrom, boolean[][] arrayTo) {

        for (int i = 0; i < arrayFrom.length; i++) {
            arrayTo[i] = Arrays.copyOf(arrayFrom[i], arrayFrom[i].length);
        }
    }
}
