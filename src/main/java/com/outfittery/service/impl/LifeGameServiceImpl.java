package com.outfittery.service.impl;

import com.outfittery.core.LifeGameLogic;
import com.outfittery.service.LifeGameService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by nickc on 9/8/16.
 */
public class LifeGameServiceImpl implements LifeGameService {


	@Autowired
	LifeGameLogic lifeGameLogic;

	@Override
	public boolean[][] initLifeGrid(int size) {
		return lifeGameLogic.initLifeGrid();
	}

	@Override
	public boolean[][] getNextGeneration() {
		return new boolean[0][];
	}
}
