package com.outfittery.service;

/**
 * Created by nickc on 9/8/16.
 */
public interface LifeGameService {

	boolean[][] initLifeGrid(int size);


	boolean[][] getNextGeneration();


}
