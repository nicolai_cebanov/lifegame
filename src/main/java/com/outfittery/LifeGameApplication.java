package com.outfittery;

import com.outfittery.core.LifeGameLogic;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LifeGameApplication {

	public static void main(String[] args) {
		new LifeGameLogic().initBoard();
//		SpringApplication.run(LifeGameApplication.class, args);

	}
}
