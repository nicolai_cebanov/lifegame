package com.outfittery.api.rest;

import com.outfittery.service.LifeGameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by nickc on 9/8/16.
 */
@RestController
@RequestMapping("/api/")
public class LifeGameController {

	@Autowired
	LifeGameService lifeGameService;


	@RequestMapping(value = "/{size}", method = RequestMethod.GET, produces = "application/json")
	public boolean[][] initLifeGrid(@PathVariable(value = "size") Integer size) {

		return lifeGameService.initLifeGrid(size);
	}

	@RequestMapping(method = RequestMethod.GET, produces = "application/json")
	public boolean[][] getNextGeneration() {

		return lifeGameService.getNextGeneration();
	}


}
